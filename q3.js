// Write a pattern that matches e-mail addresses.

const isValidEmail = (email)=>{
   let reg = /^([a-zA-Z0-9\.\-]+)@([a-zA-Z0-9\.\-]+)\.([a-zA-Z]){2,4}$/i;
   console.log(email.match(reg));
}

let email = "lakshay@gmail.com";
let result = isValidEmail(email);

console.log(`is ${email} valid ? - ${result}`);
