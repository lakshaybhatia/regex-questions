// Write a JavaScript program to search a date within a string.

// Sample string:

const isDateStr = (str) =>
{
    const reg = /^(1[0-2]|0?[1-9])\/(3[01]|[12][0-9]|0?[1-9])\/(?:[0-9]{2})?[0-9]{2}$/g;
    console.log(str.match(reg));
    return reg.test(str);
}

let dateStr = "lakshay was born on 12/1/1999";
console.log(`is ${dateStr} has date string : ${isDateStr(dateStr)}`)