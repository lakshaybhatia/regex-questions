// Write a JavaScript function to check whether a given value is alpha numeric or not.

const isAlphaNumeric = (str)=>{
    let reg = /^[A-Za-z0-9]+$/;
   return reg.test(str)
}

let str = "3243sew";
console.log(`is ${str} alphanumeric? - ${isAlphaNumeric(str)}`);